# insert_table_materiels_values.py
# AL 20.03.2020 insertion table materiel

import connect_db
'''
Importer le fichier "InsertOneTable" dans lequel il y a quelques classes et
méthodes en rapport avec le sujet d'insertion dans UNE SEULE table.
'''
from INSERT import insert_one_table

try:
    # AL 20.03.2020 création d'un objet "etre_connecte"
    objet_etre_connecte = connect_db.DatabaseTools()

    '''
    AL 20.03.2020 Une instance "insert_records" pour permettre l'utilisation
    des méthodes de la classe DbInsertOneTable
    '''
    insert_records = insert_one_table.DbInsertOneTable()

    # AL 20.03.2020 déclaration de la variable valeur_debile_mais_presque_aleatoire_a_inserer
    valeur_debile_mais_presque_aleatoire_a_inserer = "bon week-end à vous"
    print(valeur_debile_mais_presque_aleatoire_a_inserer)

    '''
    insert_records.insert_one_record_one_table("INSERT IGNORE INTO t_genres
    (id_genre, intitule_genre) VALUES (null, %(values_insert)s)",
    valeur_debile_mais_presque_aleatoire_a_inserer)
    '''
    insert_records.insert_one_record_one_table("INSERT INTO `t_materiels` (`id_Materiel`, `fk_TypeMateriel`, `MarqueMateriel`, `ArticleMateriel`) VALUES (NULL, '6', 'Berkley', 'Big Game')", valeur_debile_mais_presque_aleatoire_a_inserer)

    # AL 20.03.2020 fermeture de la BD.
    objet_etre_connecte.close_connection()
except Exception as erreur_merdique:
    print("il y a une erreur {0}", erreur_merdique)
